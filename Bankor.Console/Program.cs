﻿using Microsoft.FSharp.Core;

using System;
using System.Diagnostics;
using Bankor.Lib;
using static Bankor.Lib.Domain;

namespace Bankor.Terminal
{
  class Program
  {
    static void Main(string[] args)
    {
      var checksAcc = new Account(1, "Checks", 2_000M, 150M, 0M);
      var savingsAcc = new Account(2, "Savings", 10_000M, 1_500M, 0M);

      var res = Operations.Withdraw(2_020M, checksAcc);
      checksAcc = PrintResult(checksAcc, res);

      var res2 = Operations.Withdraw(30M, checksAcc);
      PrintResult(checksAcc, res2);

      var res3 = Operations.Withdraw(5_500M, savingsAcc);
      PrintResult(savingsAcc, res3);

      var res4 = Operations.Deposit(120M, checksAcc);
      PrintResult(checksAcc, res4);

      var res5 = Operations.Deposit(7_800M, savingsAcc);
      PrintResult(checksAcc, res5);
      var err = DepositError.NegativeBalanceProvided;
      var abc = err switch
      {
        DepositError error when error == DepositError.NegativeBalanceProvided => "Negative",
        DepositError error when error == DepositError.UndisclosedError => "Undisclosed",
        _ => "why Though?"
      };
      Debug.WriteLine(abc);
    }

    private static Account PrintResult(Account account, FSharpResult<Account, string> res)
    {
      if (res.IsOk)
      {
        account = res.ResultValue;
        Debug.WriteLine(account);
      }
      else
      {
        Debug.WriteLine($"Error \"{res.ErrorValue}\"");
      }

      return account;
    }
  }
}
