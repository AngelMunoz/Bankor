# Bankor

Example of how to combine C# with F#

## Requirements
- install .netcore3.1 https://dotnet.microsoft.com/download/dotnet-core/3.1

## Run

`dotnet run -p Bankor.Terminal`


## for vscode 
- https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csharp
- https://marketplace.visualstudio.com/items?itemName=Ionide.Ionide-fsharp