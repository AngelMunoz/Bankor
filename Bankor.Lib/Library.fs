﻿namespace Bankor.Lib

module Domain = 
    type Profile = 
        { Id: int
          Name: string 
          LastName: string 
          BirthDate: string
          Accounts: list<int> }
    type Account =
        { Id: int
          Name: string
          Amount: decimal
          AllowedOverdraft: decimal
          CurrentOverdraft: decimal }

    type Operation =
        | Withdrawal of decimal
        | Deposit of decimal

    [<RequireQualifiedAccess>]
    type WithdrawalResult = 
        | WithdrawalSuccess of decimal
        | AllowedOverdraft of account: decimal * overdraft: decimal

    [<RequireQualifiedAccess>]
    type WithdrawalError =
        | NegativeBalanceProvided
        | UndisclosedError
        | EmptyAccount
        | DisallowedOverdraft of decimal

    [<RequireQualifiedAccess>]
    type DepositResult = 
        | Allowed of decimal

    [<RequireQualifiedAccess>]
    type DepositError =
        | NegativeBalanceProvided
        | UndisclosedError

module Operations =
    open Domain

    let private attemptWithdrawal (amount: decimal) (account: Account) : Result<WithdrawalResult, WithdrawalError> =
        let accAmountPlusOverdraft = account.Amount + account.AllowedOverdraft
        match account.Amount with
        | 0M -> 
            Error WithdrawalError.EmptyAccount
        | accamount when accamount < 0M ->
            Error WithdrawalError.NegativeBalanceProvided
        | accamount when accamount >= amount ->
            Ok (WithdrawalResult.WithdrawalSuccess amount)
        | accamount when amount <= accAmountPlusOverdraft ->
            Ok (WithdrawalResult.AllowedOverdraft (accamount, accamount - (amount - account.AllowedOverdraft)))
        | accamount ->
            match accamount with 
            | _ when amount > accAmountPlusOverdraft ->
                Error (WithdrawalError.DisallowedOverdraft (accAmountPlusOverdraft - amount))
            | _ -> Error WithdrawalError.UndisclosedError

    let private withdrawal (account: Account) (attemptResult: Result<WithdrawalResult, WithdrawalError>) : Result<Account, string> =
        match attemptResult with 
        | Ok result ->
            match result with 
            | WithdrawalResult.WithdrawalSuccess amount ->
                Ok { account with Amount = account.Amount - amount }
            | WithdrawalResult.AllowedOverdraft (amount, overdraft) -> 
                Ok { account with Amount = account.Amount - amount; CurrentOverdraft = overdraft * -1M }
        | Error err ->
            match err with 
            | WithdrawalError.EmptyAccount -> Error "The account is empty"
            | WithdrawalError.DisallowedOverdraft amount -> 
                let msg = 
                    sprintf "The amount overdraft (%.2f) excedes the allowed account overdraft (%.2f)" amount account.AllowedOverdraft
                Error msg
            | WithdrawalError.NegativeBalanceProvided ->  Error "the withdrawal requested is a negative value"
            | WithdrawalError.UndisclosedError ->  Error "An undiscosed error happened"

    let private attemptDeposit (amount: decimal) (account: Account) : Result<DepositResult, DepositError> = 
        match amount with
        | accamount  when accamount < 0M -> 
            Error (DepositError.NegativeBalanceProvided)
        | amount -> 
            Ok (DepositResult.Allowed amount)

    let private deposit (account: Account) (result: Result<DepositResult, DepositError>) : Result<Account, string> = 
        match result with 
        | Ok result ->
            match result with 
            | DepositResult.Allowed amount ->
                Ok { account with Amount = account.Amount + amount }
        | Error err ->
            match err with 
            | DepositError.NegativeBalanceProvided -> Error "The balance provided must be positive"
            | DepositError.UndisclosedError ->  Error "An undisclosed error happened"

    let Withdraw (amount: decimal) (account: Account) : Result<Account, string> = 
        let withdrawWithAccount = withdrawal account
        account 
        |> attemptWithdrawal amount
        |> withdrawWithAccount

    let Deposit (amount: decimal) (account: Account) : Result<Account, string> =
        let depositWithAccount = deposit account
        account 
        |> attemptDeposit amount
        |> depositWithAccount


